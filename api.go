package main

import (
	"bytes"
	"cloud.google.com/go/firestore"
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/kiwicom/go-sls-examples/burja-cloudrun/pkgs/fs"
	"google.golang.org/api/iterator"
	"log"
	"net/http"
	"os"
	"time"
)

type ResponseBody struct {
	Stats map[string]ResponseItem `json:"stats"`
}

type ResponseItem struct {
	City      string    `json:"city"`
	WindSpeed float64   `json:"windSpeed"`
	WindGust  float64   `json:"windGust"`
	Lat       float64   `json:"lat"`
	Lng       float64   `json:"lng"`
	UpdatedAt time.Time `json:"updatedAt"`
}

func ListMeasurements(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	results, err := getResults(ctx, -7)
	if err != nil {
		handleResponse(w, ResponseBody{}, http.StatusInternalServerError, err)
		return
	}

	// return empty results
	if len(results) == 0 {
		handleResponse(w, ResponseBody{}, http.StatusOK, nil)
		return
	}

	responseBody, err := prepareBody(results)
	if err != nil {
		handleResponse(w, ResponseBody{}, http.StatusInternalServerError, err)
		return
	}

	handleResponse(w, responseBody, http.StatusOK, nil)
}

func getResults(ctx context.Context, days int) ([]*firestore.DocumentSnapshot, error) {
	client, err := fs.Init(ctx)
	if err != nil {
		return nil, err
	}

	windM := client.Collection("WindMeasurementsCR")

	q := windM.Where("time", ">=", time.Now().AddDate(0, 0, days).Unix())

	var results []*firestore.DocumentSnapshot

	iter := q.Documents(ctx)
	defer iter.Stop()
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		results = append(results, doc)
	}

	defer client.Close()

	return results, nil
}

func handleResponse(w http.ResponseWriter, responseBody ResponseBody, statusCode int, err error) {
	if err != nil {
		handleErrorResponse(w, statusCode, err)
		return
	}

	body, err := json.Marshal(responseBody)
	if err != nil {
		handleErrorResponse(w, http.StatusInternalServerError, err)
		return
	}

	var buf bytes.Buffer
	json.HTMLEscape(&buf, body)

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(buf.Bytes())
}

func handleErrorResponse(w http.ResponseWriter, statusCode int, err error) {
	w.WriteHeader(statusCode)
	w.Write([]byte(err.Error()))
}

func prepareBody(results []*firestore.DocumentSnapshot) (ResponseBody, error) {
	firstItem, err := valueToItem(results[0])
	if err != nil {
		return ResponseBody{}, err
	}

	body := ResponseBody{}
	body.Stats = map[string]ResponseItem{}
	body.Stats["min_wind_speed"] = firstItem
	body.Stats["max_wind_speed"] = firstItem

	for _, resultItem := range results {
		item, err := valueToItem(resultItem)
		if err != nil {
			return ResponseBody{}, err
		}

		if item.WindSpeed < body.Stats["min_wind_speed"].WindSpeed {
			body.Stats["min_wind_speed"] = item
		}

		if item.WindSpeed > body.Stats["max_wind_speed"].WindSpeed {
			body.Stats["max_wind_speed"] = item
		}
	}

	return body, nil
}

func valueToItem(resultItem *firestore.DocumentSnapshot) (ResponseItem, error) {
	city, err := resultItem.DataAt("city")
	if err != nil {
		return ResponseItem{}, fmt.Errorf("failed to fetch the data: %v", err)
	}

	windSpeed, err := resultItem.DataAt("windSpeed")
	if err != nil {
		return ResponseItem{}, fmt.Errorf("failed to fetch the data: %v", err)
	}

	windGust, err := resultItem.DataAt("windGust")
	if err != nil {
		return ResponseItem{}, fmt.Errorf("failed to fetch the data: %v", err)
	}

	lat, err := resultItem.DataAt("lat")
	if err != nil {
		return ResponseItem{}, fmt.Errorf("failed to fetch the data: %v", err)
	}

	lng, err := resultItem.DataAt("lng")
	if err != nil {
		return ResponseItem{}, fmt.Errorf("failed to fetch the data: %v", err)
	}

	updatedTime, err := resultItem.DataAt("time")
	if err != nil {
		return ResponseItem{}, fmt.Errorf("failed to fetch the data: %v", err)
	}

	item := ResponseItem{}
	item.City = city.(string)
	item.WindSpeed = windSpeed.(float64)
	item.WindGust = windGust.(float64)
	item.Lat = lat.(float64)
	item.Lng = lng.(float64)
	item.UpdatedAt = time.Unix(updatedTime.(int64), 0)

	return item, nil
}

func main() {
	http.HandleFunc("/", ListMeasurements)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}
