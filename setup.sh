#!/usr/bin/env bash

source .env
export PROJECT_ID=$PROJECT_ID

gcloud config set project $PROJECT_ID
gcloud services enable cloudbuild.googleapis.com
gcloud services enable run.googleapis.com
gcloud services enable datastore.googleapis.com
gcloud services enable firestore.googleapis.com
gcloud services enable cloudscheduler.googleapis.com
gcloud config set run/region us-central1
gcloud config unset run/cluster
gcloud config unset run/cluster_location
gcloud pubsub topics create burja || true
gcloud app create --region=europe-west3 || true
