package fs

import (
	"cloud.google.com/go/firestore"
	"context"
	"github.com/caarlos0/env"
)

type config struct {
	ProjectId string `env:"PROJECT_ID"`
}

func Init(ctx context.Context) (*firestore.Client, error) {
	cfg := config{}
	err := env.Parse(&cfg)
	if err != nil {
		return nil, err
	}

	client, err := firestore.NewClient(ctx, cfg.ProjectId)
	if err != nil {
		return nil, err
	}

	return client, nil
}
