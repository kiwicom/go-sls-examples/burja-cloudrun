module gitlab.com/kiwicom/go-sls-examples/burja-cloudrun

go 1.12

require (
	cloud.google.com/go v0.40.0
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/stretchr/testify v1.3.0 // indirect
	google.golang.org/api v0.6.0
)
