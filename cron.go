package main

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/kiwicom/go-sls-examples/burja-cloudrun/pkgs/fs"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

type ResponseData struct {
	RoutingVersion int       `json:"RoutingVersion"`
	Updated        int       `json:"updated"`
	ModifiedTime   time.Time `json:"ModifiedTime"`
	Copyright      string    `json:"copyright"`
	ModelVersion   int       `json:"ModelVersion"`
	Expires        time.Time `json:"Expires"`
	Contents       []struct {
		ModifiedTime time.Time `json:"ModifiedTime"`
		Language     string    `json:"Language"`
		IsModified   bool      `json:"IsModified"`
		ContentName  string    `json:"ContentName"`
		Expires      time.Time `json:"Expires"`
		ETag         string    `json:"ETag"`
		Data         struct {
			ContentName string `json:"ContentName"`
			Language    string `json:"Language"`
			Items       []struct {
				YWgs        float64 `json:"y_wgs"`
				Description string  `json:"Description"`
				Title       string  `json:"Title"`
				ContentName string  `json:"ContentName"`
				XWgs        float64 `json:"x_wgs"`
				CrsID       string  `json:"CrsId"`
				Sunki       float64 `json:"sunki"`
				Veter       float64 `json:"veter"`
				Y           float64 `json:"Y"`
				X           float64 `json:"X"`
				ID          string  `json:"Id"`
				Icon        string  `json:"Icon"`
			} `json:"Items"`
		} `json:"Data"`
	} `json:"Contents"`
}

type WindMeasurement struct {
	Id        string
	City      string  `firestore:"city"`
	WindSpeed float64 `firestore:"windSpeed"`
	WindGust  float64 `firestore:"windGust"`
	Lat       float64 `firestore:"lat"`
	Lng       float64 `firestore:"lng"`
	Time      int64   `firestore:"time"`
}

func FetchMeasurements(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	m, err := parse()
	if err != nil {
		handleResp(w, http.StatusInternalServerError, err)
		return
	}

	store(ctx, w, m)

	handleResp(w, http.StatusOK, nil)
}

func parse() (measurements []WindMeasurement, err error) {
	client := &http.Client{Timeout: 10 * time.Second}

	r, err := client.Get("https://opendata.si/promet/burja/")
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()

	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	data := ResponseData{}

	err = json.Unmarshal(b, &data)
	if err != nil {
		return nil, err
	}

	for _, item := range data.Contents[0].Data.Items {
		measurements = append(measurements, WindMeasurement{
			Id:        item.ID + "-" + strconv.Itoa(data.Updated),
			City:      item.Title,
			WindSpeed: item.Veter,
			WindGust:  item.Sunki,
			Lat:       item.X,
			Lng:       item.Y,
			Time:      data.ModifiedTime.Unix(),
		})
	}

	return measurements, nil
}

func store(ctx context.Context, w http.ResponseWriter, measurements []WindMeasurement) {
	client, err := fs.Init(ctx)
	if err != nil {
		handleResp(w, http.StatusInternalServerError, err)
		return
	}

	windM := client.Collection("WindMeasurementsCR")

	for _, m := range measurements {
		wm := windM.Doc(m.Id)

		_, err := wm.Create(ctx, m)
		if err != nil {
			handleResp(w, http.StatusInternalServerError, err)
			return
		}
	}

	defer client.Close()
}

func handleResp(w http.ResponseWriter, statusCode int, err error) {
	if err != nil {
		handleErrorResp(w, statusCode, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte("All good!"))
}

func handleErrorResp(w http.ResponseWriter, statusCode int, err error) {
	w.WriteHeader(statusCode)
	w.Write([]byte(err.Error()))
}

func main() {
	http.HandleFunc("/", FetchMeasurements)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}
