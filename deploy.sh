#!/usr/bin/env bash

source .env
export PROJECT_ID=$PROJECT_ID

gcloud builds submit . --config=cloudbuild.yml

gcloud beta run deploy burja-api --image gcr.io/${PROJECT_ID}/burja_api --set-env-vars PROJECT_ID=${PROJECT_ID} --allow-unauthenticated
CRON_URL=$(gcloud beta run deploy burja-cron --image gcr.io/${PROJECT_ID}/burja_cron --set-env-vars PROJECT_ID=${PROJECT_ID} --allow-unauthenticated 2>&1 >/dev/null | grep -Eo 'https://[^ >]+' | head -1)

gcloud beta scheduler jobs create http burja-cr --schedule="0 * * * *" --uri="${CRON_URL}" --time-zone="Europe/Prague" --http-method=GET | true
