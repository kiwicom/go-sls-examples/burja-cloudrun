.PHONY: lint deploy

lint:
	golint -set_exit_status

deploy:
	./deploy.sh
